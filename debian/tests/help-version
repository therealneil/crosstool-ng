#!/bin/sh
# shellcheck disable=SC2317
# crosstool-ng/debian/tests/help-version

set -euvx

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}"'['"$$"']:' "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    exit 1
}

################################################################################
################################################################################
################################################################################

# '/usr/share/dpkg/default.mk' includes '/usr/share/dpkg/pkg-info.mk' and
# '/usr/share/dpkg/pkg-info.mk' defines seven extremely useful
# variables. Re-implement those GNU Make things here.
DEB_SOURCE="${DEB_SOURCE:-$(dpkg-parsechangelog --file="${here}"/../changelog --show-field=Source)}"
DEB_VERSION="${DEB_VERSION:-$(dpkg-parsechangelog --file="${here}"/../changelog --show-field=Version)}"
DEB_VERSION_EPOCH_UPSTREAM="${DEB_VERSION_EPOCH_UPSTREAM:-$(echo "${DEB_VERSION}" | sed -e 's/-[^-]*$//')}"
DEB_VERSION_UPSTREAM_REVISION="${DEB_VERSION_UPSTREAM_REVISION:-$(echo "${DEB_VERSION}" | sed -e 's/^[0-9]*://')}"
DEB_VERSION_UPSTREAM="${DEB_VERSION_UPSTREAM:-$(echo "${DEB_VERSION_EPOCH_UPSTREAM}" | sed -e 's/^[0-9]*://')}"
DEB_DISTRIBUTION="${DEB_DISTRIBUTION:-$(dpkg-parsechangelog --file="${here}"/../changelog --show-field=Distribution)}"
SOURCE_DATE_EPOCH="${SOURCE_DATE_EPOCH:-$(dpkg-parsechangelog --file="${here}"/../changelog --show-field=Timestamp)}"
SOURCE_DATE="$(date -uIseconds -d@"${SOURCE_DATE_EPOCH}")"
info DEB_SOURCE: "${DEB_SOURCE}"
info DEB_VERSION: "${DEB_VERSION}"
info DEB_VERSION_EPOCH_UPSTREAM: "${DEB_VERSION_EPOCH_UPSTREAM}"
info DEB_VERSION_UPSTREAM_REVISION: "${DEB_VERSION_UPSTREAM_REVISION}"
info DEB_VERSION_UPSTREAM: "${DEB_VERSION_UPSTREAM}"
info DEB_DISTRIBUTION: "${DEB_DISTRIBUTION}"
info SOURCE_DATE_EPOCH: "${SOURCE_DATE_EPOCH}"
info SOURCE_DATE: "${SOURCE_DATE}"

################################################################################

# Assert that the first line of the 'help' message reports the proper upstream
# version.
have_help_first="$(ct-ng help 2>/dev/null | head -n1)"
need_help_first="This is crosstool-NG version ${DEB_VERSION_UPSTREAM}"
if ! [ "${have_help_first}" = "${need_help_first}" ]; then
    die \
        help_first: \
        '\n' have: "${have_help_first}" \
        '\n' need: "${need_help_first}"
fi

exit "$?"
